<!DOCTYPE html>
<!-- <html manifest="{{ url('site.manifest') }}"> -->
<html>
@include('components.metaInformation')

<body class="main">
    @include('components.loader')
    @include('components.navbar.index')

    <main class="container-fluid">
        @yield('container')
    </main>
    <!-- Modal -->
    <div class="modal fade" id="forgot_password_modal" aria-labelledby="forgot_password_modal"
        aria-hidden="true">
        <div class="modal-dialog" style="z-index:99!important">
            <div class="modal-content">
                <div class="modal-header bg-dark text-white">
                    <h5 class="modal-title" id="forgot_password_modal">Atualizar senha</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="success_message" class="d-none">
                        <i class="material-icons">check</i> <strong>Sucesso: </strong> Suas informações foram salvas
                    </div>
                    <form name="form_new_password_modal">
                        <div class="input-block settings">
                            <label for="Nome" class="text-dark">Senha Atual: </label>
                            <input name="current_password" class="form-confirm" autocomplete type="password" id="current_password">
                        </div>
                        <div class="input-block settings">
                            <label for="Nome" class="text-dark">Senha Nova: </label>
                            <input name="new_password" autocomplete class="form-confirm" type="password" id="new_password">
                        </div>
                        <div class="input-block settings">
                            <label for="Nome" class="text-dark">Confirmar Senha Nova: </label>
                            <input name="confirm_new_password" autocomplete class="form-confirm" type="password" id="confirm_new_password">
                        </div>
                        <button class="button-back pointer show" type="button" id="change_password_button">
                            Alterar Senha
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @yield('show-script')
    <script src="https://kit.fontawesome.com/9f635a5f2f.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
    <script src="https://code.responsivevoice.org/responsivevoice.js?key=GxDuvDFM"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/navbar.js') }}" defer></script>
    <script src="{{ asset('js/croppie.js') }}" defer></script>
    <script src="{{ asset('js/flickity.pkgd.min.js') }}" defer></script>
    <script src="{{ asset('js/image-upload.js') }}" defer></script>
    <script src="{{ asset('js/voice.js') }}" defer></script>
    <script src="{{ asset('js/tracking-min.js') }}" defer> </script>
    <script src="{{ asset('js/face-min.js') }}" defer> </script>
    <script src="{{ asset('js/reconhecimento.js') }}" defer> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dat-gui/0.7.6/dat.gui.js"> </script>
    <script src="{{ asset('service-worker.js') }}"></script>
    <script>
        $("#btn-add-settings-submit").on("click", function() {
            event.preventDefault();
            $.ajax({
                url: '{{ url('confirm_pass') }}',
                type: "post",
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: {
                    'password': $("#password_confirmation").val()
                },
            }).then(function(response) {
                if (response == 1) {
                    $(".main-nav-container").removeClass("confirm");
                    $(".main-nav-container").attr("class",
                        "main-nav-container full-height settings");
                    $("#password_confirmation").val('');
                    $('.alert-confirmation-password').addClass('d-none');
                } else {
                    $('.alert-confirmation-password').removeClass('d-none');
                }
            });
        });

        $(".toggle-switch").on('click', function() {
            var self = $(this);
            let dataToggle = $(this).attr('data');
            $.ajax({
                url: '{{ url('settings_toggle') }}',
                type: "post",
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: {
                    'toggle': dataToggle
                },
            }).then(function(response) {
                self.toggleClass("deactive-toggle-switch");
            });
        });

        $("#update_name_email").on('click', function() {
            var self = $(this);
            let object = $('#form_update_name_email').serialize();
            let xurl = '{{ route('alterar_email_senha') }}';
            xurl += '?' + object

            $.ajax({
                url: xurl,
                type: 'get',
                headers: { 'X-CSRF-Token': '{{ csrf_token() }}', },
                data: object
            }).then(function(response) {
                $('#form_update_name_email div input').val('');
                $('#success_message').removeClass('d-none');
                setTimeout(function() {
                    $('#success_message').addClass('d-none');
                }, 3000);
            });

        });


        $('#change_password_button').on('click',function(){
            let current_password = $('#current_password').val();
            let new_password = $('#new_password').val();
            let confirm_new_password = $('#confirm_new_password').val();

            let object = {
                current_password: current_password,
                new_password: new_password,
                confirm_new_password: confirm_new_password,
            }

            $.ajax({
                url: '{{ route('alterar_senha') }}',
                type: 'post',
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: object
            }).then(function(response) {
                $('#form_update_name_email div input').val('');
                $('#success_message').removeClass('d-none');
                setTimeout(function() {
                    $('#success_message').addClass('d-none');
                }, 3000);

                console.log(response);
            });
        });
        


    </script>

    @yield('service-worker')

</body>

</html>
