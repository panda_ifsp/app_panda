<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('page_title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/croppie.css') }}" rel="stylesheet">
    <link href="{{ asset('css/flickity.min.css') }}" rel="stylesheet">


    <meta name="mobile-web-app-capable" content="yes">
	<link rel="icon" sizes="200x200" href="{{ asset('imgs/icons/panda-bear.svg') }}">
	<meta name="theme-color" content="#FFF">
	<link rel="manifest" href="{{asset("manifest.json")}}">
	<!-- fim android -->

    <!-- manifest -->
    <link rel="manifest" href="{{ asset('mix-manifest.json') }}">
</head>
<body class="main">
    @include('components.loader')
    <main class="container-fluid">
        @yield('container')
    </main>
<!-- Scripts -->
<!--  TODO  -->
<!--  colocar o jquery local -->
@yield('show-script')
<script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>

<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/dropdown.js') }}" defer></script>
<script src="{{ asset('js/croppie.js') }}" defer></script>
<script src="{{ asset('js/flickity.pkgd.min.js') }}" defer></script>
<script src="{{ asset('js/image-upload.js') }}" defer></script>
<script src="{{ asset('js/service-worker.js') }}" defer></script>
<script src="{{ asset('js/loader.js') }}" defer></script>
<script src="{{ asset('js/voice.js') }}" defer></script>
<!-- <script src="{{ asset('js/web-cache-local-storage.js') }}"></script> -->
<script src="{{ asset('service-worker.js') }}"></script>
<script>
        window.onload = function(){
            setTimeout(function() { $("#loader").fadeOut() },1000);
        }
    </script>
@yield('service-worker')
</body>
</html>