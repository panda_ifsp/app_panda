@extends('layouts.main')


@section('page_title')

{{'Dashboard'}}

@endsection

@extends('components.navbar')

@section('content')
@endsection

@section('show-script')
<script src="{{ asset('js/exibir-livros.js') }}"></script>
@endsection

@section('service-worker')
    @if(auth()->check())
        <script>
            if ('serviceWorker' in navigator ) {
                window.addEventListener('load', function() {
                    navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
                        // Registration was successful
                        console.log('ServiceWorker registration successful with scope: ', registration.scope);
                    }, function(err) {
                        // registration failed :(
                        console.log('ServiceWorker registration failed: ', err);
                    });
                });
            }

            function fala(){
                $('.speak-desc').each( function(){
                    var fala = $(this).attr("alt");
                    setTimeout(responsiveVoice.speak(fala,"Brazilian Portuguese Male",{volume: 1}),1000);
                    console.log("carregando vozes ...");
                });
            }

            $(function(){
                // var falas = [];
                fala();
            })
        </script>
    @endif
@endsection