<!DOCTYPE html>
<html>
<head>
  <title>ProsaIF - Projeto de Sistema Assistivo do Instituto Federal</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="{{asset('css/materialize.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/hover-min.css')}}">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <meta 
     name='viewport' 
     content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' 
/>
</head>
<body>
  <nav class="z-depth-0">
    <div class="nav-wrapper white">
      <div class="container-fluid px-1">
        <a href="#" class="brand-logo"><img src="{{asset('img/logo_light_icon.png')}}" width="120" style="margin-top: 0.9rem;"></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li class="hvr-underline-from-left"><a href="#head" class="grey-text">Inicio</a></li>
          <li class="hvr-underline-from-left"><a href="#whatis" class="grey-text">O que é?</a></li>
          <li class="hvr-underline-from-left"><a href="#comp_tablet" class="grey-text">Como funciona</a></li>
          <li class="hvr-underline-from-left"><a href="#precos" class="grey-text">Preços</a></li>
          <li class="hvr-underline-from-left"><a href="#planos" class="grey-text">Planos</a></li>
          <li class="hvr-underline-from-left"><a href="/login" class="grey-text">Login</a></li>
          <li class="hvr-underline-from-left"><a href="/register" class="grey-text">Faça parte</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container center pt-10" id="head">
    <img src="{{asset('img/logo_light.png')}}" class="responsive-img" alt="">
    <!-- <h5 class="">Projeto de Sistema Assistivo do Instituto Federal</h5> -->
	<div class="hide-on-med-and-up">
		<a href="/register" style="background: #00ae0b !important;" class="btn-large mt-4">Criar minha conta</a>
		<a href="/login" class="btn-large white black-text mt-4">Entrar</a>
	</div>
  </div>
  <div class="container-fluid" id="whatis">

    <div class="row">
      <div class="col m6">
        <img src="{{asset('img/welcome/bg1.png')}}" class="hide-on-med-and-down" data-aos="fade-right" data-aos-duration="1000" data-aos-offset="400">
      </div>
      <div class="col m6 mt-6">
        <h1 class="f_700 " data-aos="fade-left" data-aos-duration="1000" data-aos-offset="200">O que é o ProsaIF?</h1>
        <div class="row">
          <div class="col m10">
            <p class="description" data-aos="fade-left" data-aos-duration="1000" data-aos-offset="300">Esse projeto é inovador e surge da oportunidade de incluir pessoas com necessidades especiais que não se comunicam pela fala, mas que não apresentam limitação psicomotora total. É um sistema que integra um ambiente em nuvem computacional com aplicativo para dispositivos móveis em que, tanto a pessoa com necessidades especiais quanto seus familiares possam criar livros baseados em imagens para que eles se comuniquem. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid" id="comp_tablet">
    <div class="container ">
      <div class="row pt-5">
        <h3 class="center pb-6" data-aos="fade-right" data-aos-duration="1000" data-aos-offset="200">Como funciona?</h3>
        <div class="col m4 center">
          <div class="row">
            <i class="material-icons large "  data-aos="fade-up" data-aos-duration="600">accessibility</i>
          </div>
          <p data-aos="fade-up" data-aos-duration="900">É um App assistivo <strong class="">(Tecnologia Assistiva)</strong> voltado ao auxilio na <strong>inclusão social</strong> de pessoas com <strong>necessidades especiais</strong>.</p>
        </div>
        <div class="col m4 center">
          <div class="row">
            <i class="material-icons large " data-aos="fade-up" data-aos-duration="1300">record_voice_over</i>
          </div>
          <p data-aos="fade-up" data-aos-duration="1300"> É desenvolvido <strong>como um livro digital</strong> com imagens e textos que, quando selecionados, <strong>são lidos pelo dispositivo</strong>. </p>
        </div>
        <div class="col m4 center">
          <div class="row">
            <i class="material-icons large  text-darken-2" data-aos="fade-up" data-aos-duration="1600">book</i>
          </div>
          <p data-aos="fade-up" data-aos-duration="1600">Esse livro é de construção dinâmica e individual, que integra as experiências do usuário com as que estão em seu cotidiano.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid hide" id="precos">
    <div class="container">
      <div class="divider my-4"></div>
      <div class="row center">
        <i class="material-icons medium " data-aos="fade-up" data-aos-duration="1000">favorite_border</i>
        <h1 class="large f_600" data-aos="fade-right" data-aos-duration="1000" >É Gratuito!</h1>
        <p class="md-text col m6 offset-m3" data-aos="fade-down" data-aos-duration="1000">O Panda é <strong>de graça</strong>, e prezamos que todos possam criar um livro e utilizá-lo a qualquer momento, <strong>basta se cadastrar!</strong></p>
      </div>
      <div class="divider my-4"></div>
    </div>
    <div class="container hide">
      <div class="row center" id="planos">
        <i class="material-icons medium "  data-aos="fade-up" data-aos-duration="1000">attach_money</i>
        <h1 class="f_600 center " data-aos="fade-right" data-aos-duration="1000">Planos</h1>
        <p class="center md-text" data-aos="fade-down" data-aos-duration="1000">Planos e preços de utilização</p>
        <div class="divider my-4"></div>
        <div class="row">
          <div class="col m6  offset-m3">
            <p class="md-text" data-aos="fade-right" data-aos-duration="1000"><strong>Mas não era gratuito?!</strong></p>
            <p class="md-text" data-aos="fade-left" data-aos-duration="1000">Sim! Mas além da utilização grátis do Panda <strong>você pode contratar</strong> um dos nossos planos! Eles cabem no seu bolso e se moldam a forma <strong>que você utiliza o app!</strong> :)</p>
          </div>
        </div>
      </div>
    </div>

    <div class="row hide">
      <div class="col m10 offset-m1">
        <div class="row">
          <div class="col m4">
            <div data-aos="fade-up" data-aos-duration="600" data-aos-offset="200" class="card green white-text card-small">
              <div class="card-content">
                <span class="card-title activator">Plano <strong>Jovem Panda</strong><i class="material-icons right">add</i>
                  <p class="mt-2">Com este plano você entra no mundo Panda e se torna um <strong>Jovem Panda</strong>, e como todo Panda, tem direito a <strong>um livro</strong>, ele é seu, basta fazer seu <strong>cadastro</strong></p>
                </span>
                <p>*O livro do Jovem Panda possui limite de 10MB, o que permite, em média, até 20 imagens</p>
              </div>
              <div class="card-reveal grey-text text-darken-2">
                <span class="card-title "><i class="material-icons right">close</i></span>
                <p class="black-text">Pacote do Plano Gratuito</p>
                <p> - Um livro (com armazenamento de até 20MB).</p>
                <p> - Disponibilidade offline (em modo APP) de seus livros.</p>
                <p> - Conta individual.</p>
              </div>
            </div>
          </div>
          <div class="col m4">
            <div data-aos="fade-up" data-aos-duration="1200" data-aos-offset="200" class="card blue darken-3 white-text card-small">
              <div class="card-content">
                <span class="card-title activator">Plano <strong>Panda Adulto</strong><i class="material-icons right">more_vert</i>
                  <p class="mt-2">Como Panda Adulto você tem direito a quantos livros quiser, e lógico, como todo adulto pode fazer parte da <strong class="italic">Panda Staff</strong>, onde você se junta com outros Pandas próximos a você!</p>
                </span>
                <p>*Os livros deste plano suportam até 100 imagens</p>
                <p>*O preço mensal para ser um Panda Adulto é de R$ 1,99</p>
              </div>
            </div>
          </div>
          <div class="col m4">
            <div data-aos="fade-up" data-aos-duration="2000" data-aos-offset="200" class="card red darken-4 white-text card-small">
              <div class="card-content">
                <span class="card-title activator">Plano <strong>Panda Sênior</strong><i class="material-icons right">more_vert</i>
                  <p class="mt-2">O <strong>Panda Sênior</strong> pode tudo (ou quase)! Sendo um Sênior, você pode entrar em contato conosco e ter características feitas sob medida para as suas necessidades</p>  
                </span>
              </div>
            </div>
          </div>
        </div> <!-- fecha row -->
      </div>
    </div>
    <!-- <div class="container">
      <div class="divider my-4"></div>
      <div class="row" >
        <div class="col m12 ">
         <p class="md-text" data-aos="fade-left" data-aos-duration="1000"><strong>Como assim?</strong></p>
         <p class="md-text " data-aos="fade-right" data-aos-duration="1000">O Panda baseia seus preços e planos na quantidade de <strong>cards</strong> e livros que o usuário coloca em sua conta, permitindo o <strong>upgrade</strong> da sua conta em <strong>qualquer momento</strong>, é você quem decide!</p>
       </div> 
     </div>
   </div> -->
 </div>
 <div class="container-fluid mt-10 grey lighten-3 py-3 hide">
  <div class="container">
    <div class="row">
      <div class="col m8">
        <h1 class="f_700 blue-text" data-aos="fade-right" data-aos-duration="1000">Quer entrar no mundo ProsaIF? Muita calma! Podemos te ajudar :)</h1>
        <p class="md-text" data-aos="fade-right" data-aos-duration="1000">Pensando em você, damos uma oportunidade incrível para você conhecer os diferentes limites do Panda e até onde ele pode te levar.</p>
        <p class="md-text" data-aos="fade-right" data-aos-duration="1000">Se você não fez parte ainda do universo ProsaIF, ou se tornou apenas um Jovem, você pode contratar 10 contas do Plano Adulto por três meses apenas por R$9,99.</p>
        <p data-aos="fade-right" data-aos-duration="1000">*valido apenas se nunca assinou o Plano Panda Adulto ou Sênior</p>
        <a href="" class="btn btn-large blue right mt-2" data-aos="fade-right" data-aos-duration="1000"><i class="material-icons left">more</i>Saber mais</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid" id="alfa">
  <div class="row">
    <div class="col m7 right-align">
      <h1 class="f_700" data-aos="fade-right" data-aos-duration="1000" data-aos-offset="200">Faça parte</h1>
      <div class="row">
        <div class="col m6 offset-m6">
          <p class="" data-aos="fade-right" data-aos-duration="1000" data-aos-offset="200">O cadastro é <strong>simples e rápido</strong>, entre e aproveite <strong>de tudo que temos a oferecer</strong>.</p>
          <a href="" class="mt-2 btn btn-large blue darken-3" data-aos="fade-right" data-aos-duration="1000" data-aos-offset="200"><i class="material-icons left" >edit</i>Faça seu cadastro</a>
        </div>

      </div>
    </div>
    <div class="col m6 " >
      <img src="{{asset('img/welcome/bg2.png')}}" class="hide-on-med-and-down" data-aos="fade-left" data-aos-duration="1000" data-aos-offset="500">
    </div>
  </div>
</div>
 <div class="container pb-5" id="time">
      <div class="">
        <h3 class="center pb-5 pt-5" data-aos="fade-right" data-aos-duration="1000" data-aos-offset="200">Nosso time</h3>
        <div class="container">
          <div class="row">
            <div class="col m4 s6">
              <img class="responsive-img" src="https:\\panda-app.netlify.app/img/time/carlos.jpg" alt="Professor Carlos Santos">
              <p>
		<a href="http://lattes.cnpq.br/2704773293662530" target="_blank">
                	<b>Carlos H. Silva-Santos </b>
		</a>
                <br>
                Coordenador
              </p>
            </div>
            <div class="col m4 s6">
              <img class="responsive-img" src="https:\\panda-app.netlify.app/img/time/paulo.jpg" alt="Paulo Cândido">
              <p>
		<a href="http://lattes.cnpq.br/1994196517067630" target="_blank">
                	<b>Paulo H. V. Cândido </b>
		</a>
                <br>
                Full Stack Developer
              </p>
            </div>
            <div class="col m4 s6">
              <img class="responsive-img" src="https:\\panda-app.netlify.app/img/time/joao.jpg" alt="João Morais">
              <p>	
		<a href="https://www.linkedin.com/in/jo%C3%A3o-victor-ferreira-de-morais-20b774203" target="_blank">
                <b>João V. F. Morais </b>
		</a>
                <br>
                Front-end Developer
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col m4 s6">
              <img class="responsive-img" src="https:\\panda-app.netlify.app/img/time/vinicius.jpg" alt="Vinicius Zevarex">
              <p>
		<a href="https://www.linkedin.com/in/vinicius-zevarex/" target="_blank">
	            <b>Vinicius A. O. Zevarex </b>
		</a>
                <br>
                Back-end Developer
              </p>
            </div>
            <div class="col m4 s6">
              <img class="responsive-img" src="https:\\panda-app.netlify.app/img/time/lucas.jpg" alt="Vinicius Zevarex">
              <p>
                <b>Lucas F. A. Cavalherie </b>
                <br>
                Back-end Developer
              </p>
            </div>
            <div class="col m4 s6">
              <img class="responsive-img" src="https:\\panda-app.netlify.app/img/time/pedro.jpg" alt="Pedro Góis">
              <p>
                <b>Pedro H. M. de Gois</b>
                <br>
                Back-end Developer
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="container-fluid py-3 footer">
  <div class="container">
    <div class="row">
      <h2 class="white-text uppercase my-5 thin" data-aos="fade-up">"a oportunidade de incluir pessoas com necessidades especiais que não se comunicam pela fala"</h2>
    </div>
    <div class="row uppercase white-text">
      <div class="col m3" data-aos="fade-left" data-aos-duration="1000">
        <p>ProsaIF</p>
        <ul>
          <li><a target="_blank" href="https://www.br-ie.org/pub/index.php/rbie/article/view/v29p25">Sobre nós</a></li>
        </ul>
      </div>
      <div class="col m3" data-aos="fade-left" data-aos-duration="1000">
        <p>Nos contate</p>
        <ul>
          <li><a href="https://www.br-ie.org/pub/index.php/rbie/article/view/v29p25" target="_blank">Formas de contato</a></li>
        </ul>
      </div>
      <div class="col m3" data-aos="fade-left" data-aos-duration="1000">
        <p>Colabore</p>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid grey darken-4 grey-text lighten-3 py-1" id="footer-2">
  <div class="container center">
    <p>© ProsaIF 2020 - Todos os direitos reservados</p>
  </div>
</div>
</body>
<script type="text/javascript" src="{{asset('js/welcome/jquery.js"')}}></script>
<script type="text/javascript" src="{{asset('js/welcome/bin/materialize.min.js"')}}></script>
<!--<script type="text/javascript" src="{{asset('js/welcome/aos.js"')}}></script> -->
<script type="text/javascript" src="{{asset('js/welcome/main.js"')}}></script>
</html>
