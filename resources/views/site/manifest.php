CACHE MANIFEST
<?php
	echo "#". auth()->user()->v_cache ."\n";

?>
CACHE:
<?php
echo asset('/css/app.css')."\n";
echo asset('/css/croppie.css')."\n";
echo asset('/css/flickity.min.css')."\n";
echo asset('/css/swiper.min.css')."\n";
echo asset('/img/icons/add-button-inside-black-circle.svg')."\n";
echo asset('/img/icons/back.svg')."\n";
echo asset('/img/icons/book.svg')."\n";
echo asset('/img/icons/books.svg')."\n";
echo asset('/img/icons/cancel.svg')."\n";
echo asset('/img/icons/clipboard.svg')."\n";
echo asset('/img/icons/cloud-computing.svg')."\n";
echo asset('/img/icons/delete.svg')."\n";
echo asset('/img/icons/edit.svg')."\n";
echo asset('/img/icons/error.svg')."\n";
echo asset('/img/icons/home.svg')."\n";
echo asset('/img/icons/logout.svg')."\n";
echo asset('/img/icons/panda-bear.svg')."\n";
echo asset('/img/icons/settings.svg')."\n";
echo asset('/img/icons/user.svg')."\n";
echo asset('/img/icons/wink.svg')."\n";
echo asset('/img/components/bg-register-page.jpg')."\n";
echo asset('/img/components/bg_login.jpg')."\n";
echo asset('/img/components/bg_sidebar.jpg')."\n";
echo asset('/img/components/img_teste_livro.jpg')."\n";
echo asset('/img/components/logo.png')."\n";
echo asset('/js/app.js')."\n";
echo asset('/js/croppie.js')."\n";
echo asset('/js/croppie.min.js')."\n";
echo asset('/js/exibir-livros.js')."\n";
echo asset('/js/flickity.pkgd.min.js')."\n";
echo asset('/js/image-upload.js')."\n";
echo asset('/js/loader.js')."\n";
echo asset('/js/service-worker.js')."\n";
echo asset('/js/speak_load.js')."\n";
echo asset('/js/swiper.min.js')."\n";
echo asset('/js/voice.js')."\n";
echo asset('/js/web-cache-local-storage.js')."\n";
echo asset('/js/reconhecimento-cadeira.js')."\n";
echo asset('/js/reconhecimento.js')."\n";
echo asset('/js/tracking-min.js')."\n";
echo asset('/js/tracking.js')."\n";
echo asset('/js/face-min.js')."\n";
echo asset('/js/eye.js')."\n";
echo asset('/js/face.js')."\n";
echo asset('/js/eye-min.js')."\n";
echo asset('/index.php')."\n";
// echo url('/')."\n";
// echo url('/')."/\n";
echo url('/dashboard')."\n";
// echo url('/login')."\n";

?>

NETWORK:
*

FALLBACK:
<?php 
	echo url('/dashboard')."\n";
 ?>
s
SETTINGS:
prefer-online