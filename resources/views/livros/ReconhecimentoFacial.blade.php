@extends('layouts.main')
@section('page_title')
{{'Reconhecimento Facial'}}
@endsection
@section('container')
@if(!empty($livros_fatiados))
<style>
    video,
    canvas {
        position: absolute;
        transform: rotateY(180deg);
        opacity: 0;
    }
</style>
<script>
    function tell(valor) {
        //alert(valor);
        return responsiveVoice.speak(valor);
    }

</script>
<video id="video" class="" width="400" height="240" preload autoplay loop></video>
<canvas id="canvas" class="" width="400" height="240"></canvas>
<div class="container-fluid mt-1 pt-6" id="livrosVueInstance">
    <div class="col-12 m-auto">
        <div class="row">
            <div class="{{ Auth::user()->swype ? 'carousel' : '' }}  col-12"
                data-flickity='{ "wrapAround": true, "draggable":false}'>
                <div class="carousel-cell container-fluid" v-for="(livros_all, indice) in livrosVueContentFatiados"
                    :key="indice">
                    <div class="row">
                    <div class="col-12 col-md-4 col-sm-12 livro" v-for="livro in livros_all" :key="livro.id">
                            <div class="card">
                                <label :for="'falar_' + livro.id " class="pointer book-container">
                                    <img :src="livro.img_data" class="card-img-top livro-img-data">
                                    <h5 class="livro-descricao card-title">
                                        {!! '&#123;&#123; livro.descricao &#125;&#125;' !!}
                                    </h5>
                                </label>
                                <a :href="'/dashboard?father='+ livro.id" v-if="livro.folha === false">
                                    <input :id="'falar_'+  livro.id" class="d-none speak-desc"
                                        @click="responsiveVoiceFalar(livro.descricao, livro.folha)" type="button" value="fala"
                                        :alt="livro.descricao" />
                                </a>
                                <input :id="'falar_' + livro.id" class="d-none speak-desc" v-else
                                    @click="responsiveVoiceFalar(livro.descricao, livro.folha)" type="button" value="fala"
                                    :alt="livro.descricao" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="container-fluid">
            <div class="row align-items-center h-100vh">
                <div class="col-12 col-md-6 m-auto pt-5 self-align-center">
                    <img class="m-auto mb-3" src="{{ asset('img/icons/panda-bear.svg') }}"
                        style="width: 10rem; display:block;">
                    <h3 class="text-center">Você ainda não possui livros</h3>
                    <p class="text-center">Vamos mudar isso?</p>
                    <div class="row justify-content-center">
                        <a class="btn btn-primary col-12 col-xl-6 col-lg-8 col-md-12 m-auto p-1"
                            href="{{ route('create_livro') }}">Adicionar um Novo
                            Livro</a>
                    </div>
                </div>
            </div>
        </div>
@endif

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>

        window.onload = function() {
            fala();
        }

        new Vue({
            el: '#livrosVueInstance',
            data: () => ({
                livrosVueContent: {!! json_encode($livros_all) !!},
                speakFatherBook: {!! Auth::user()->speak !!},
                livrosVueContentFatiados:[],
                isMobile:false
            }),
            mounted:function(){
                let self = this;
                this.generateBooksSlice();
                this.verifyIsMobile();
                window.addEventListener('resize', function(event){
                    console.log(window.innerWidth);
                    self.generateBooksSlice();
                    self.verifyIsMobile();
                });
                
            },
            methods: {
                responsiveVoiceFalar: function(string, folha) {
                    if(folha && this.speakFatherBook == 0){
                       return;
                    }
                    window.responsiveVoice.speak(string, "Brazilian Portuguese Male");
                },
                generateBooksSlice: function(){
                    var counterNumb = 6;
                    var livrosFatiados = [];

                    if (window.innerWidth < 992) {
                        counterNumb = 2
                    }else if(window.innerWidth < 768){
                        counterNumb = 2
                    }else{
                        counterNumb = 6
                    }

                    for (var i = 0; i < this.livrosVueContent.length; i += counterNumb) {
                        livrosFatiados.push(this.livrosVueContent.slice(i, (i + counterNumb)));
                    }

                    this.livrosVueContentFatiados = livrosFatiados;
                    $('.carousel').flickity('destroy');
                    $('.carousel').flickity();
                },
                verifyIsMobile:function(){
                    this.isMobile = (window.innerWidth < 992) ? true : false;
                }
            }
        });

    </script>

@endsection
