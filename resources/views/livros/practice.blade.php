@extends('layouts.main')
@section('page_title')
{{'Dashboard'}}
@endsection
@section('container')
<style>
    video,
    canvas {
        position: absolute;
        transform: rotateY(180deg);
        /* opacity: 0; */
    }
</style>
<div class="container-fluid mt-5">
    <div class="col-md-12">

        <div class="row justify-content-center align-items-center pt-10">
            <div class="col-md-4 col-sm-4">
                <div class="row">
                    <div class="col-6">
                        <h1>PosicaoX: <span id="posicao_cabeca_x"></span></h1>
                    </div>
                    <div class="col-6">
                        <h1>PosicaoY: <span id="posicao_cabeca_y"></span></h1>
                    </div>
                </div>
                <h1 id="position_index">Posicao</h1>
                <div class="card mx-auto">
                    <video id="video" class="" width="400" height="240" preload autoplay loop></video>
                    <canvas id="canvas" class="" width="400" height="240"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

</script>

@endsection