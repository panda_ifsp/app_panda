@extends('layouts.main')

@section('page_title')
{{'Novo Livro'}}
@endsection
@section('container')
<div class="create carousel justify-content-center align-items-center mt-3" data-flickity='{ "draggable": false, "selectedAttraction": 0.01,
"friction": 0.15, "pageDots": false, "contain":true, "asNavFor": ".create"}'>
    <div class=" carousel-cell col-12 add-book-carousel first">
        <div class="container text-center">
            <img id="create_cloud_icon" class="m-auto" width="100" height="200" src="{{asset('img/icons/cloud-computing.svg')}}" alt="ícone de núvem com uma seta de upload" />
            <h2 class="f_600 text-center ">Primeiro adicione uma Imagem!</h2>
            <p class="text-center ">Será através das imagens que você irá utilizar o nosso sistema</p>
            <div class="col-md-6 col-lg-4 m-auto mt-3 mb-3" for="book_figure">
                <button id="btn_upload_img" class="btn btn-primary col-12 p-1">Fazer Upload da Imagem!</button>
            </div>
            <form action="upload.php" method="post" style="display:none" enctype="multipart/form-data">
                <input type="file" id="input_book_figure" name="book_figure">
            </form>
        </div>
   </div>
    <div class="carousel-cell col-12 add-book-carousel second">
        <div class="row pb-6 pt-5">
            <div class="col-12 col-md-8 col-lg-6 m-auto offset-md-1">
                <div>
                    <img src="{{asset('img/components/bg_login.jpg')}}" alt="Imagem selecionada pelo usuário" id="preview-img">
                </div>
            </div>
            <div class="col-9 col-md-8 col-lg-6 m-auto text-align-left form-adicionar-livro">
                <h3 class="f_600">Primeiro ajuste sua imagem ao lado</h3>
                <p>É muito importante que você ajuste a imagem da forma que a verá nos livros do Panda</p>
                <h3 class="mt-1 f_600">Agora nos diga o que ela é:</h3>
                <div class="form-group">
                    <!-- <label for="descricao">E-mail</label> -->
                    <input type="text" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" id="input_descricao" aria-describedby="descricao" placeholder="Pote de Mel" name="descricao" value="" required autofocus>
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('descricao') }}</strong>
                    </span>
                </div>
                <p class="mt-2">Quando tiver certeza de todas as informações e se a imagem estiver correta conclua o processo de criação do seu livro:</p>

                <form action="{{route('store_livro')}}" method="POST" name="form_create_livro" id="form_create_livro">
                    @csrf
                    <input class="d-none" type="hidden" value="{{isset($_GET['father'])? $_GET['father'] : NULL}}" name="idPai" id="idPai" required>
                    <input class="d-none" type="text" value="" name="img_data" id="input_create_livro_img_data" required>
                    <input class="d-none" type="text" value="" name="description" id="input_create_livro_description" required>
                    <button type="submit" disabled=true id="btn_create_livro_submit" class="btn btn-primary col-12 p-1" onclick='responsiveVoice.speak("")'>Tudo pronto!</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
        document.querySelector('body').scrollTo(0,0);
</script>
@endsection

@extends('components.modal')

@section('modal_header')
{{'Calma, alguma coisa deu errado'}}
@endsection
@section('modal_body')
{{'Verifique se todas as informações que digitou estão corretas e se a imagem foi adicionada corretamente'}}
@endsection
@section('modal_btn')
{{'Entendido!'}}
@endsection
