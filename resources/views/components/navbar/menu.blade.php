<nav class="nav-container">
    <a href="{{ route('dashboard') }}">
        <img class="panda-icon" src="{{ asset('img/logo_dark_icon.png') }}" alt="logo_da_aplicação">
    </a>
    <i class="far fa-user user-icon show pointer"></i>
</nav>

<nav class="nav-menu">
    <div class="header-menu">
        <img class="panda-icon" src="{{ asset('img/logo_dark_icon.png') }}" alt="logo_da_aplicação">
        <span class="menu-title"> Menu </span>
        <img class="close-icon hide pointer" src="{{ asset('img/icons/close-icon.svg') }}" alt="icone_para_fechar_menu">
    </div>

    <div class="menu-container">
        <a href="{{ route('dashboard') }}">
            <div class="card-menu pointer">
                <i class="fas fa-home"></i>
                <p class="menu-description"> Início </p>
            </div>
        </a>
        <a href="{{ route('edit') }}">
            <div class="card-menu pointer">
               <i class="far fa-edit"></i>
                <p class="menu-description"> Editar </p>
            </div>
        </a>
        <a href="#">
            <div class="card-menu confirm pointer">
                <i class="fas fa-cogs"></i>
                <p class="menu-description"> Alterar Dados </p>
            </div>
        </a>
        <a href="{{ route('face_recognition') }}">
            <div class="card-menu pointer">
                <i class="fas fa-video"></i>
                <p class="menu-description"> Controle por <br> movimento </p>
            </div>
        </a>
        <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <div class="card-menu pointer">
                <i class="fas fa-door-open"></i>
                <p class="menu-description"> Sair </p>
            </div>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</nav>
