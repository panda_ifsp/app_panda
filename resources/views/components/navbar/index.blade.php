<div class="main-nav-container">
    @include('components.navbar.menu')
    @include('components.navbar.confirmation')
    @include('components.navbar.settings')
</div>
