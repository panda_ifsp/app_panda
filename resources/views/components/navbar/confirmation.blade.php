  <nav class="nav-menu-confirm">
            <div class="header-menu">
                <div class="back show pointer">
                    <img class="back-icon" src="{{asset('img/icons/back-icon.svg')}}" alt="icone_para_voltar_anteriormente">
                    <span class="back-description"> Voltar </span>
                </div>
                <span class="menu-title"> Confirmação </span>
                <img class="close-icon hide pointer" src="{{asset('img/icons/close-icon.svg')}}" alt="icone_para_fechar_menu">
            </div>

            <div class="menu-container-confirm">
                <header class="header-confirm">
                    <div class="header-desc">
                        <h1 class="confirm-title"> Confirmação de Usuário </h1>
                        <span class="description"> Para acessar esse ambiente, você precisa confirmar sua identidade</span>
                    </div>
                    <img class="icone-avatar-confirm" src="{{asset('img/icons/account-details-outline-icon.svg')}}" alt="ícone_avatar">
                </header>
                <form class="form-section" id="form-section-confirm">
                    @csrf
                    <div class="input-block">
                        <label for="senha"> Senha </label>
                        <input class="form-confirm form-input-text-type" autocomplete type="password" id="password_confirmation" name="password">
                    </div>
                    <p class="text-white alert-confirmation-password d-none">A senha não confere</p>
                    <div class="input-block">
                        <button id="btn-add-settings-submit" class="btn-add pointer" type="button">
                            Continuar
                            <img class="nex-img-button" src="{{asset('img/icons/next-icon.svg')}}" alt="confirmar_senha">
                        </button>
                    </div>
                </form>
                <div class="input-block">
                    <button class="button-back pointer show" type="button">
                        <img class="back-img-button" src="{{asset('img/icons/preview-icon.svg')}}" alt="voltar_página_anterior">
                        Voltar
                    </button>
                </div>
            </div>
        </nav>
