<nav class="nav-menu-settings">
    <div class="header-menu">
        <div class="back show pointer">
            <img class="back-icon" src="{{ asset('img/icons/back-icon.svg') }}" alt="icone_para_voltar_anteriormente">
            <span class="back-description"> Voltar </span>
        </div>
        <span class="menu-title"> Configurações </span>
        <img class="close-icon hide pointer" src="{{ asset('img/icons/close-icon.svg') }}" alt="icone_para_fechar_menu">
    </div>

    <div class="menu-container-settings">
        <div class="user-settings">
            <header class="header-confirm absolute">
                <div class="header-desc-settings">
                    <h1 class="confirm-title settings-title"> Configurações de Usuário </h1>
                    <span class="description settings-desc"> Altere as principais informações de acesso e utilização da
                        conta </span>
                </div>
                <img class="icone-avatar-confirm" src="{{ asset('img/icons/account-details-outline-icon.svg') }}"
                    alt="ícone_avatar">
            </header>

            <form class="form-section" action="" method="post" id="form_update_name_email">
                <div class="input-block settings">
                    <label for="Nome"> Nome </label>
                    <input name="nome" class="form-confirm" type="text" id="Nome" value="{{ Auth::user()->name }}">
                </div>

                <div class="input-block">
                    <label for="Email"> Email </label>
                    <input name="email" class="form-confirm" type="email" id="Email" value="{{ Auth::user()->email }}">
                </div>
                <div id="success_message" class="d-none">
                    <i class="material-icons">check</i> <strong>Sucesso: </strong> Suas informações foram salvas
                </div>
                <div id="forgot-password">
                    <a href="#" data-toggle="modal" data-target="#forgot_password_modal">Alterar Senha!</a>
                </div>
                <div class="input-block">
                    <button class="btn-add pointer settings-button" type="button" id="update_name_email">
                        Atualizar
                    </button>
                </div>
            </form>

        </div>

        <div class="user-settings">
            <header class="">
                <div class="header-desc">
                    <h1 class="confirm-title settings-title"> Configurações da Interface </h1>
                    <span class="description settings-desc"> Utilizar o movimento de deslizar para navegar entre os
                        livros </span>
                </div>
            </header>
            <div class="toggle-settings">

                <div class="item-settings">
                    <img class="toggle-settings-img toggle-switch {{ Auth::user()->swype ? '' : 'deactive-toggle-switch' }}"
                        data="swype" src="{{ asset('img/icons/toggle-icon.svg') }}" alt="opção_configuração">
                    <div class="input-block-desc">
                        <h1 class="setting-title"> Deslizar </h1>
                        <span class="settings-description description"> Utilizar o movimento de deslizar para navegar
                            entre os livros </span>
                    </div>
                </div>

                <div class="item-settings">
                    <img class="toggle-settings-img toggle-switch {{ Auth::user()->speak ? '' : 'deactive-toggle-switch' }}"
                        src="{{ asset('img/icons/toggle-icon.svg') }}" alt="opção_configuração" data="speak">
                    <div class="input-block-desc">
                        <h1 class="setting-title"> Falar </h1>
                        <span class="settings-description description"> Sintetizar fala ao clicar na capa dos livros
                        </span>
                    </div>
                </div>

                <div class="item-settings">
                    <img class="toggle-settings-img toggle-switch {{ Auth::user()->inverted ? '' : 'deactive-toggle-switch' }}"
                        src="{{ asset('img/icons/toggle-icon.svg') }}" alt="opção_configuração" data="invert">
                    <div class="input-block-desc">
                        <h1 class="setting-title"> Inverter Cores </h1>
                        <span class="settings-description description"> Trocar o tom de cores do sistema </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>



