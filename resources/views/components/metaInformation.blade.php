<head>
    <title>@yield('page_title')</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
<!-- Styles -->
@if(Auth::user()->inverted == 0)
<link href="{{ asset('css/global.css') }}" rel="stylesheet">
@else
<link href="{{ asset('css/inverted.css') }}" rel="stylesheet">
@endif
<link href="{{ asset('css/livros.css') }}" rel="stylesheet">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/navbar.css') }}" rel="stylesheet">
<link href="{{ asset('css/croppie.css') }}" rel="stylesheet">
<link href="{{ asset('css/flickity.min.css') }}" rel="stylesheet">
<meta name="mobile-web-app-capable" content="yes">
<link rel="icon" sizes="200x200" href="{{ asset('imgs/icons/panda-bear.svg') }}">
<meta name="theme-color" content="#FFF">
<link rel="manifest" href="{{asset('manifest.json')}}">
<!-- fim android -->

    <!-- manifest -->
    <link rel="manifest" href="{{ asset('mix-manifest.json') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
    <script src="{{ asset('js/flickity.pkgd.min.js') }}" defer></script>




	<meta name="description" content="ProsaIF - Projeto de Sistema Assistivo do Instituto Federal"/>
	<meta name="keywords" content="ifsp, instituto federal de são paulo, prosaIF, prosa, IF"/>
	<meta name="author" content="Instituto Federal de São Paulo"/>
	<meta name="description" content="Projeto de Sistema Assistivo do Instituto Federal"/>
	<meta name="subject" content="ProsaIF - Projeto de Sistema Assistivo do Instituto Federal">
	<meta name="copyright" content="ProsaIF">
	<meta name="language" content="pt-BR">
	<meta name="robots" content="index,follow" />
	<meta name="revised" content="Tuesday, October 27th, 2020, 5:15 pm" />
	<meta name="abstract" content="Projeto de Sistema Assistivo do Instituto Federal">
	<meta name="topic" content="Sistema Assistivo">
	<meta name="summary" content="">
	<meta name="Classification" content="Sistema Assistivo">
	<meta name="designer" content="Instituto Federal de São Paulo">
	<meta name="reply-to" content="carlos.santos@ifsp.edu.br">
	<meta name="owner" content="Instituto Federal de São Paulo">
	<meta name="url" content="http//prosaif.itp.ifsp.edu.br">
	<meta name="identifier-URL" content="http//prosaif.itp.ifsp.edu.br">
	<meta name="directory" content="/">
	<meta name="category" content="Sistema Assistivo">
	<meta name="coverage" content="Worldwide">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="imagetoolbar" content="no" />
</head>
