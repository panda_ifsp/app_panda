<div class="modal fade" tabindex="-1" id="alert_modal" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center">
        <div class="container">
          <div class="row justify-content-center">
            <img src="{{asset('img/icons/error.svg')}}" width="100px" alt="Icone de erro">
          </div>
          <div class="row text-center">
            <h2 class="f_800">@yield('modal_header')</h2>
          </div>
        </div>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <div class="modal-body text-center">
        <p class="f_600 p-1">@yield('modal_body')</p>
        <div class="text-center">
          <button type="button" class="btn btn-danger p-1 px-3 mt-2 mb-2" data-dismiss="modal">@yield('modal_btn')</button>
        </div>
      </div>
    </div>
  </div>
</div>