@extends('layouts.auth')
@section('page_title')
{{'Login'}}
@endsection
@section('container')
<div class="col-md-6 col-12 m-auto form-box">
	<div class="col-6 col-sm-5 col-md-6 col-lg-5 m-auto">
		<img class="img-fluid panda" src="{{asset('img/logo_light.png')}}">
	</div>
	<form method="POST" action="{{ route('login') }}">
		@csrf
		<div class="form-group">
			<label for="email">E-mail</label>
			<input
				type="email"
				class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
				id="email"
				aria-describedby="email"
				placeholder="usuario@dominio.com"
				name="email"
				value="{{ old('email') }}"
				required
				autofocus
			>
			@if ($errors->has('email'))
			<span class="invalid-feedback" role="alert">
				<strong>{{ $errors->first('email') }}</strong>
			</span>
			@endif
		</div>
		<div class="form-group">
			<label for="password">Senha</label>
			<input
				type="password"
				class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
				id="password"
				placeholder="*********"
				name="password"
				required
				>

			@if ($errors->has('password'))
			<span class="in		<script>
						window.onload = function(){
							setTimeout(function() { $(" #loader").fadeOut() },1000); } </script>valid-feedback" role="alert">
				<strong>{{ $errors->first('password') }}</strong>
			</span>
			@endif
		</div>

		{{-- <div class="form-group form-check mb-2">
				<div class="col-md-12 text-right">
					@if (Route::has('password.request'))
					<a class="" href="{{ route('password.request') }}">
						{{ __('Esqueceu sua senha?') }}
					</a>
					@endif
				</div>
		</div> --}}
		<button type="submit" class="mt-2 btn btn-primary col-12 p-1">Entrar</button>
		<p class="text-center mt-1">Clique para acessar ao sistema ou realize o seu <a href="{{url('register')}}"><b>cadastro</b></a> </p>
		<p class="text-center mt-2 mb-5"><a href="{{route('welcome')}}">Voltar</a></p>
	</form>

</div>
@endsection
