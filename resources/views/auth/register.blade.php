@extends('layouts.auth')
@section('page_title')
{{'Cadastrar'}}
@endsection
@section('container')
<div class="col-md-5 m-auto">
    <div class="col-8 col-md-4 m-auto text-center">
        <img class="img-fluid panda" width="100px" src="{{asset('img/icons/clipboard.svg')}}">
    </div>
    <h1 class="text-center">Faça o seu cadastro abaixo!</h1>
    <p class="mb-3 text-center">Ao se tornar um Proseador você poderá criar livros e utilizá-los onde e quando quiser!</p>
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="form-group">
            <label for="nome">Nome: </label>
            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="nome" aria-describedby="nome" placeholder="José Colméia" value="{{ old('name') }}" name="name" required autofocus>

            @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>

        <div class="form-group">
            <label for="email">E-mail:</label>
            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" aria-describedby="email" placeholder="zecolmeia@bears.com" value="{{ old('email') }}" name="email" required>

            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>

        <div class="form-group">
            <label for="password">Senha: </label>
            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" placeholder="********">

            @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif

        </div>

        <div class="form-group">
            <label for="password-confirm">Confirmar Senha: </label>
            <input type="password" class="form-control" id="password-confirm" name="password_confirmation" placeholder="*******" required>
        </div>

        {{-- <div class="form-group form-check mb-5">
			<input type="checkbox" class="form-check-input" id="exampleCheck1">
			<label class="form-check-label" for="exampleCheck1">Check me out</label>
		</div>  --}}
        <p class="text-center mt-3">Ao clicar você fará parte do nosso time!</p>
        <button type="submit" class="btn btn-primary col-12 p-1">Começar a Prosear!</button>
        <p class="text-center text-gray mb-1 mt-1 ">Se encontrar problemas durante seu cadastro entre <br>em contato com o <a href="{{route("suporte")}}">suporte</a></p>
        <p class="text-center mt-1 mb-5"><a href="{{route('welcome')}}">Voltar</a></p>

    </form>
</div>
@endsection