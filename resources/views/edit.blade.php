@extends('layouts.main')


@section('page_title')

    {{ 'Dashboard' }}

@endsection

@section('container')

    <div class="container-fluid pt-6 mt-1">
        <div class="col-md-12">
            <div class="row">
                @foreach ($livros_all as $livro)

                    <div class="col-12 col-md-4 col-sm-12 livro">
                        <div class="card pointer">
                            <div class="book-container">
                                <img src="{{ $livro->img_data }}" class="card-img-top livro-img-data">
                                <h5 class="card-title livro-descricao">
                                    {{ $livro->descricao }}
                                </h5>
                                <img src="{{ asset('/img/icons/pen.svg') }}" alt="Editar" class="btn-edit"
                                    onclick="handleEditViewShow(event)">
                                <div class="edit-view d-none">
                                    <img src="{{ asset('/img/icons/close.svg') }}" alt="Fechar"
                                        onclick="handleEditViewHide(event)" class="btn-close-edit-view pointer">
                                    <div class="subtitle">
                                        Editar livro
                                    </div>
                                    <form action="{{ route('update') }}" method="get" class="contain-input">
                                        <textarea id="book_description" rows="1" onfocus="handleEditViewActionsShow(event)"
                                            name="descricao">{{ $livro->descricao }}</textarea>
                                        <input type="hidden" value="{{ $livro->id }}" name="id" class="d-none livro-id">
                                        <button type="submit" class="btn btn-primary d-none"
                                            id="submit-description-update-form_{{ $livro->id }}"> Alterar Nome </button>

                                        <label for="book_description">
                                            <img src="{{ asset('/img/icons/pen.svg') }}" alt="Editar"
                                                class="btn-edit-view-edit pointer">
                                        </label>
                                    </form>
                                    <a class="btn add-child" href="{{ route('edit', ['father' => $livro->id]) }}">
                                        <img src="{{ asset('/img/icons/plus.svg') }}" alt="Adicionar Filho"
                                            class=" pointer">
                                        Adicionar Filho
                                    </a>
                                    <div class="actions-livro d-none">
                                        <form name="excluir_livro" id="form_delete_book"
                                            action="{{ route('destroy_livro') }}" method="POST">
                                            @csrf
                                            <input type="hidden" value="{{ $livro->id }}" name="id" class="d-none livro-id">
                                            <button class="d-none" type="submit"
                                                id="btn-excluir-livro_{{ $livro->id }}">Excluir Livro</button>
                                        </form>
                                        <label for="btn-excluir-livro_{{ $livro->id }}">
                                            <a class="btn red">
                                                <img src="{{ asset('/img/icons/delete_icon.svg') }}" alt="Excluir"
                                                    class=" pointer">
                                                Excluir
                                            </a>
                                        </label>
                                        <label for="submit-description-update-form_{{ $livro->id }}">
                                            <a class="btn">
                                                <img src="{{ asset('/img/icons/check.svg') }}" alt="Salvar"
                                                    class=" pointer">
                                                Salvar
                                            </a>
                                        </label>

                                    </div>
                                    <a class="cancelar-edicao-btn d-none" href="#"
                                        onclick="handleEditViewActionsHide(event)">Cancelar</a>
                                </div>
                                <div class="card-body d-none">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <form action="{{ route('update') }}" method="get">
                                                        <input type="text" name="descricao"
                                                            placeholder="{{ $livro->descricao }}">
                                                        <input type="hidden" value="{{ $livro->id }}" name="id"
                                                            class="d-none livro-id">
                                                        <button type="submit" class="btn btn-primary"
                                                            style="margin-top: 5%;"> Alterar Nome </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 text-right">
                                            <label for="btn-excluir-livro_{{ $livro->id }}">
                                                <img id="label_excluir_livro" class="img-fluid" alt="Excluir livro"
                                                    src="{{ asset('img/icons/delete.svg') }}">
                                            </label>
                                            <input id="falar_{{ $livro->id }}" class="d-none"
                                                onclick='responsiveVoice.speak("{{ $livro->descricao }}","Brazilian Portuguese Male");'
                                                type="button" value="fala" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="col-12 col-md-4 col-sm-12 livro">
                    <div class="card text-center livro-add-card">
                        <a href="{{ isset($_GET['father']) ? route('create_livro', ['father' => $_GET['father']]) : route('create_livro') }}"
                            id="novo_livro">
                            <img src="{{ asset('img/icons/add-button-inside-black-circle.svg') }}" height="100px"
                                class="my-1">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title">Adicionar Novo Livro</h5>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        function handleEditViewShow(event) {
            var cardItems = $(event.path[0]).parent().children();
            $.each(cardItems, function(elem, content) {
                if ($(content).attr("class").indexOf("edit-view") > -1) {
                    $(content).removeClass("d-none");
                }
            });
        }

        function handleEditViewHide(event) {
            var cardItems = $(event.path[0]).parent();
            $.each(cardItems, function(elem, content) {
                if ($(content).attr("class").indexOf("edit-view") > -1) {
                    $(content).addClass("d-none");
                }
            });
        }

        function handleEditViewActionsShow(event) {
            var cardItems = $(event.path[0]).parent().parent().children();
            $.each(cardItems, function(elem, content) {
                if ($(content).attr("class").indexOf("actions-livro") > -1) {
                    $(content).removeClass("d-none");
                } else if ($(content).attr("class").indexOf("add-child") > -1) {
                    $(content).addClass("d-none");
                } else if ($(content).attr("class").indexOf("cancelar-edicao-btn") > -1) {
                    $(content).removeClass("d-none");
                }
            });
        }

        function handleEditViewActionsHide(event) {
            var cardItems = $(event.path[0]).parent().children();
            console.log(cardItems);
            $.each(cardItems, function(elem, content) {
                if ($(content).attr("class").indexOf("actions-livro") > -1) {
                    $(content).addClass("d-none");
                } else if ($(content).attr("class").indexOf("add-child") > -1) {
                    $(content).removeClass("d-none");
                } else if ($(content).attr("class").indexOf("cancelar-edicao-btn") > -1) {
                    $(content).addClass("d-none");
                }
            });
        }

    </script>
@endsection
