@extends('layouts.main')


@section('page_title')

{{'Dashboard'}}

@endsection

@extends('components.navbar')

@section('content')
<div class="container-fluid mt-1">
    <div class="col-md-12">
        <div class="row">
            @foreach($livros_all as $livro)

            <div class="col-md-4 col-sm-4 livro">
                <label for="falar_{{$livro->id}}">
                    <div class="card">
                        <!-- Imagem do Livro Blob -->
                        <img src="{{$livro->img_data}}" class="card-img-top livro-img-data">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-9">
                                    <!-- descricao -->
                                    <h5 class="card-title livro-descricao">{{$livro->descricao}}</h5>
                                </div>	

                                <div class="col-md-3 text-right">
                                    <form name="excluir_livro" id="form_delete_book" action="{{route('destroy_livro')}}" method="POST">
                                        @csrf

                                        <!-- Livro ID -->
                                        <input type="hidden" value="{{$livro->id}}" name="id" class="d-none livro-id">
                                        <button class="d-none" type="submit" id="btn-excluir-livro_{{$livro->id}}">Excluir Livro</button>
                                    </form>
                                    <label for="btn-excluir-livro_{{$livro->id}}">
                                        <img id="label_excluir_livro" class="img-fluid" alt="Excluir livro" src="{{asset('img/icons/delete.svg')}}">
                                    </label>
                                    <input id="falar_{{$livro->id}}" class="d-none" onclick='responsiveVoice.speak("{{$livro->descricao}}","Brazilian Portuguese Male");' type="button" value="fala" />

                                </div>
                            </div>

                        </div>
                    </div>
                </label>
            </div>
        </div>
        @endforeach
    </div>

    <div class="col-md-3 mt-1" style="height: 321px">
        <div class="card text-center">
            <a href="{{route('create_livro')}}" id="novo_livro">
                <img src="{{ asset('img/icons/add-button-inside-black-circle.svg')}}" height="100px" class="m-auto my-4">
            </a>
            <div class="card-body">
                <h5 class="card-title">Adicionar Novo Livro</h5>
            </div>
        </div>

    </div>
</div>
</div>

@endsection