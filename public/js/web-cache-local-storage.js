//pegar o id do livro
//pegar o nome do livro
//pegar a imagem que está em blob

var $img_livro_class    = $('.livro-img-data'),
$desc_livro_class       = $('.livro-descricao'),
$id_livro_class         = $('.livro-id'),
desc_livro              = new Array(),
img_livro               = new Array(),
id_livro                = new Array();
var livros_json         = "";

function pegarDadosLivro(){
    //pegando imagens
    $img_livro_class.each(function(){
        var img_data = $(this).attr('src');
        img_livro.push(img_data);
    });

    //pegando nomes
    $desc_livro_class.each(function(){
        var desc_data = $(this).text();
        desc_livro.push(desc_data);
    });

    //pegando ID
    $id_livro_class.each(function(){
        var id_data = $(this).attr('value');
        id_livro.push(id_data);
    });

    //criando JSON
    livros_json = '{"livros":['
    for(i = 0;i < id_livro.length;i++){
        livros_json += '{'
        livros_json += '"id"' + ':"' + id_livro[i] + '"' + ",";
        livros_json += '"img_data"' + ':"' +  img_livro[i] + '"' + ",";
        livros_json += '"descricao"' + ':"' + desc_livro[i] + '"';
        livros_json += "}";

        if(i != id_livro.length - 1){
            livros_json += ",";
        }
    }
    livros_json += ']}';

    return livros_json;
}


function armazenarDadosEmCache(index,value){
    if (typeof(Storage) !== "undefined") {
        localStorage.setItem(index, value);
    } 
}


function exibirDadosLivros(livros){

} 

$(function(){
    livros_dados        = pegarDadosLivro();
    index_livros_dados  = "dados_livros"
    armazenarDadosEmCache(index_livros_dados,livros_dados);

    
    //pegando dados cache e convertendo em objetos
    livros_json = localStorage.getItem(index_livros_dados);
    livros_obj  = JSON.parse(livros_json);
    exibirDadosLivros(livros_obj);
    console.log(livros_obj);

    
});