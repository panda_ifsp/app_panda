function reconhecimentoFacialInit() {
    var reconhecimento = 0;
    var video = document.getElementById('video');
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    var widthCanvas = $('canvas').width();
    var heightCanvas = $('canvas').height();

    var square = 200;
    var squareX = widthCanvas / 2 - square / 2;
    var squareY = heightCanvas / 2 - square / 2;
    var xFace = 0;
    var yFace = 0;

    var px = 0;
    var py = 0;

    var tracker = new tracking.ObjectTracker('face');
    tracker.setInitialScale(4);
    tracker.setStepSize(1);
    tracker.setEdgesDensity(0.1);

    tracking.track('#video', tracker, {
        camera: true,
    });

    responsiveVoice.speak("Por favor, posicione sua cabeça em direção à câmera. Tente ficar no centro da imagem", "Brazilian Portuguese Male");

    tracker.on('track', function (event) {
        event.data.forEach(function (rect) {
            context.clearRect(0, 0, canvas.width, canvas.height);
            if (reconhecimento != 1) {
                // desenho do quadrado para o usuário posicioanr sua cabeça
                context.strokeStyle = 'red';
                context.strokeRect(squareX, squareY, square, square);
                context.font = '25px Helvetica';
                context.fillStyle = "#ff0000";

                // ctx.setTransform(1, 0, 0, -1, size / 2, size / 2);
                if ((rect.x > squareX && rect.x < squareX + square) && (rect.y > squareY && rect.y < squareY + square)) {
                    reconhecimento = 1;
                    responsiveVoice.speak("Posicionamento correto", "Brazilian Portuguese Male");
                }
                //
            }
            if (reconhecimento == 1) {
                if (xFace == 0 && yFace == 0) {
                    xFace = rect.x;
                    yFace = rect.y;
                }
                context.strokeStyle = 'green';
                context.strokeRect(rect.x, rect.y, rect.width, rect.height);
                context.font = '25px Helvetica';
                context.fillStyle = "#ff0000";

                px = Math.abs(xFace - rect.x);
                py = Math.abs(yFace - rect.y);

                    // document.querySelector('#posicao_cabeca_x').innerHTML = px;
                    // document.querySelector('#posicao_cabeca_y').innerHTML = py;

                var livros = $('.livro');
                var time;
                if (px > 30 || py > 15) {
                    if (px > py) {
                        if (rect.x < xFace) {
                            if (localStorage.getItem('posicaoAnterior') == 'Direita') {
                            // if()
                                return;
                            }

                                // $("#position_index").html('Direita');
                            localStorage.setItem('posicaoAnterior', 'Direita');

                            var itemAtual = 0;
                            $('.livro').each((index, elem) => {
                                if ($(elem).hasClass('active')) {
                                    itemAtual = index;
                                }
                            });

                            console.log(itemAtual + 1);
                            $('.livro').removeClass('active');
                            $(livros[itemAtual + 1]).addClass('active');
                            if ((itemAtual >= 5 && itemAtual % 5 === 0) || itemAtual == $('.livro').length - 1) {
                                document.getElementsByClassName('flickity-prev-next-button next')[0].click();
                            }

                        } else {
                            if (localStorage.getItem('posicaoAnterior') == 'Esquerda') {
                                return;
                            }
                                // $("#position_index").html('Esquerda')
                            localStorage.setItem('posicaoAnterior', 'Esquerda');

                            var itemAtual = 0;
                            $('.livro').each((index, elem) => {
                                if ($(elem).hasClass('active')) {
                                    itemAtual = index;
                                }
                            });

                            $('.livro').removeClass('active');
                            if (itemAtual == 0) {
                                itemAtual = livros.length;
                            }

                            $(livros[itemAtual - 1]).addClass('active');
                            if (itemAtual == 0 || (itemAtual >= 6 && itemAtual % 6 === 0)) {
                                document.getElementsByClassName('flickity-prev-next-button previous')[0].click();
                            }
                        }
                    }
                    else {
                        if (rect.y < yFace) {
                            if (localStorage.getItem('posicaoAnterior') == 'Topo') {
                                return;
                            }
                                // $("#position_index").html('Topo');
                            localStorage.setItem('posicaoAnterior', 'Topo');
                            $($('.active').children().children()[0]).click();

                        } else {
                            if (localStorage.getItem('posicaoAnterior') == 'Baixo') {
                                return;
                            }
                            localStorage.setItem('posicaoAnterior', 'Baixo');
                                // $("#position_index").html('Baixo');
                            $($('.active').children().children()[0]).click();
                        }
                    }
                } else {
                        // $("#position_index").html('Centro')
                    if (localStorage.getItem('posicaoAnterior') == 'Centro') {
                        return;
                    }

                    localStorage.setItem('posicaoAnterior', 'Centro');

                    clearTimeout(time);
                }
            }
        });

    });
}
window.onload = function () {
    $('#loader').fadeOut();
    reconhecimentoFacialInit();
};
