$(document).ready(function () {
    $(".show").click(() => {
        $(".main-nav-container").removeClass("confirm");
        $(".main-nav-container").attr("class", "main-nav-container full-height");

        $("body").removeClass('main');
    });

    $(".hide").click(() => {
        $(".main-nav-container").removeClass("full-height");
        $(".main-nav-container").removeClass("confirm");
        $("body").addClass('main');
    });

    $(".confirm").click(() => {
        $(".main-nav-container").toggleClass("confirm");
    });

    $(".settings-user").click(() => {
        $(".main-nav-container").attr("class", "main-nav-container full-height settings");
    });

    $('.form-input-text-type').keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

});
