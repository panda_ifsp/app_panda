window.onload = function() {
    $('#loader').fadeOut();
    var reconhecimento = 0;
    var video = document.getElementById('video');
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    var widthCanvas = $('canvas').width();
    var heightCanvas = $('canvas').height();

    var square = 200;
    var squareX = widthCanvas / 2 - square / 2;
    var squareY = heightCanvas / 2 - square / 2;
    var xFace = 0;
    var yFace = 0;

    var px = 0;
    var py = 0;

    var tracker = new tracking.ObjectTracker('face');
    tracker.setInitialScale(4);
    tracker.setStepSize(1);
    tracker.setEdgesDensity(0.1);

    tracking.track('#video', tracker, {
        camera: true,
    });

    responsiveVoice.speak("Por favor, posicione sua cabeça dentro do quadrado vermelho", "Brazilian Portuguese Male");
    
    tracker.on('track', function(event) {
        event.data.forEach(function(rect) {
            context.clearRect(0, 0, canvas.width, canvas.height);
            if (reconhecimento != 1) {
                // desenho do quadrado para o usuário posicioanr sua cabeça
                context.strokeStyle = 'red';
                context.strokeRect(squareX, squareY, square, square);
                context.font = '25px Helvetica';
                context.fillStyle = "#ff0000";

                // ctx.setTransform(1, 0, 0, -1, size / 2, size / 2);
                
                if ((rect.x > squareX && rect.x < squareX + square) && (rect.y > squareY && rect.y < squareY + square)) {
                    reconhecimento = 1;
                    responsiveVoice.speak("Posicionamento correto", "Brazilian Portuguese Male");
                }
                //
            }
            if (reconhecimento == 1) {
                if (xFace == 0 && yFace == 0) {
                    xFace = rect.x;
                    yFace = rect.y;
                }
                context.strokeStyle = 'green';
                context.strokeRect(rect.x, rect.y, rect.width, rect.height);
                context.font = '25px Helvetica';
                context.fillStyle = "#ff0000";
                // context.fillText('x: ' + rect.x + 'px', rect.x + rect.width + 5, rect.y + 11);
                // context.fillText('y: ' + rect.y + 'px', rect.x + rect.width + 5, rect.y + 22);



                // //top
                // context.strokeStyle = 'black';
                // context.strokeRect(squareX, 200, square, square);
                // context.font = '25px Helvetica';
                // context.fillStyle = "#ff0000";

                // bottom
                // context.strokeStyle = 'black';
                // context.strokeRect(squareX, (squareY * 2), square, square);
                // context.font = '25px Helvetica';
                // context.fillStyle = "#ff0000";

                // // left
                // context.strokeStyle = 'black';
                // context.strokeRect((squareX * 1.8), squareY, square, square);
                // context.font = '25px Helvetica';
                // context.fillStyle = "#ff0000";

                // // right
                // context.strokeStyle = 'black';
                // context.strokeRect((squareX * 0.2), squareY, square, square);
                // context.font = '25px Helvetica';
                // context.fillStyle = "#ff0000";

                // //top
                // if (rect.y < 200 && (rect.x > squareX && rect.x < squareX + square)) {
                //     // responsiveVoice.speak("Topo", "Brazilian Porguese Male");
                //     responsiveVoice.speak("Para Frente", "Brazilian Portuguese Male");
                // }

                // //bottom
                // if (rect.y > 260 && (rect.x > squareX && rect.x < squareX + square)) {
                //     responsiveVoice.speak("Marcha ré", "Brazilian Portuguese Male");
                // }


                px = Math.abs(xFace - rect.x);
                py = Math.abs(yFace - rect.y);

                //left
                // if (rect.x >) {
                //     responsiveVoice.speak("Esquerda", "Brazilian Portuguese Male");
                // }
                if (px > 30 || py > 10) {
                    if (px > py) {
                        if (rect.x < xFace) {
                            $("#position_index").html('Direita')
                            // responsiveVoice.speak("Direita", "Brazilian Portuguese Male");
                        } else {
                            $("#position_index").html('Esquerda')
                            // responsiveVoice.speak("Esquerda", "Brazilian Portuguese Male");
                        }
                    } else {
                        if (rect.y < yFace) {
                            $("#position_index").html('Topo')
                            // responsiveVoice.speak("Ré", "Brazilian Portuguese Male");
                        } else {
                            $("#position_index").html('Baixo')
                            // responsiveVoice.speak("Para Frente", "Brazilian Portuguese Male");
                        }
                    }
                }else{
                    $("#position_index").html('Centro')
                }


            }
        });

    });

    var gui = new dat.GUI();
    gui.add(tracker, 'edgesDensity', 0.1, 0.5).step(0.01);
    gui.add(tracker, 'initialScale', 1.0, 10.0).step(0.1);
    gui.add(tracker, 'stepSize', 1, 5).step(0.1);
};;
