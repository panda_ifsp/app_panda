const previewImgUpload = new Croppie(document.getElementById('preview-img'), {
    enableExif: true,
    viewport: {
        width: 275,
        height: 200,
        type: 'square'
    },
    boundary: {
        height: 300,
        width: '90%',
    }
});

$('#btn_upload_img').on("click", function () {
    
    $('#input_book_figure').click()

});

$('#input_book_figure').on("change", function (e) {
    
    const fileToUpload = e.target.files.item(0);
    const reader = new FileReader();

    reader.onload = function (e) {
        var imagem_src = e.target.result;

        $('#preview-img').attr('src', imagem_src);

        previewImgUpload.bind({
            url: imagem_src
        });
    }

    reader.readAsDataURL(fileToUpload);

    $(".flickity-button.flickity-prev-next-button.next").click();
    $("#descricao").focus();

});

var input = document.getElementById('input_descricao');

input.addEventListener('input', function () {
    
    let content = this.value;
    var input_description = document.getElementById('input_create_livro_description');

    if (content.length >= 3) {
        
        document.getElementById("btn_create_livro_submit").disabled = false;
        input_description.value = content;

    } else {

        document.getElementById("btn_create_livro_submit").disabled = true;

    }

});
$("#btn_create_livro_submit").on("click", function () {
   
    previewImgUpload.result('base64').then(function(img) {
   
        document.getElementById('input_create_livro_img_data').value = img;
        $("#form_create_livro").submit();

    });
   
    return false;
});