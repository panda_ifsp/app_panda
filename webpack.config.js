

/**
 * As our first step, we'll pull in the user's webpack.mix.js
 * file. Based on what the user requests in that file,
 * a generic config object will be constructed for us.
 */
// let mix = require('./node_modules/laravel-mix/src/index');
// require(Mix.paths.mix());
// require(Mix.paths.mix());
let mix = require('laravel-mix/src/index');

let ComponentFactory = require('laravel-mix/src/components/ComponentFactory');

new ComponentFactory().installAll();

Mix.dispatch('init', Mix);

let WebpackConfig = require('./node_modules/laravel-mix/src/builder/WebpackConfig');

module.exports = new WebpackConfig().build();

let path = require('path');

let glob = require('glob');

let webpack = require('webpack');

let Mix = require('laravel-mix').config;

let webpackPlugins = require('laravel-mix').plugins;

let dotenv = require('dotenv');

let SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin'); //Our magic



/**
 * Just in case the user needs to hook into this point
 * in the build process, we'll make an announcement.
 */



/**
 * Now that we know which build tasks are required by the
 * user, we can dynamically create a configuration object
 * for Webpack. And that's all there is to it. Simple!
module.exports = new WebpackConfig().build();
 */


  module.exports.plugins.push(
    new SWPrecacheWebpackPlugin({
        cacheId: 'pwa',
        filename: 'service-worker.js',
        staticFileGlobs: ['public/**/*.{css,eot,svg,ttf,woff,woff2,js,html}'],
        minify: true,
        stripPrefix: 'public/',
        handleFetch: true,
        dynamicUrlToDependencies: {
            '/': ['resources/views/welcome.blade.php'],
            '/dashboard': ['resources/views/dashboard.blade.php']
        },
        navigateFallback: '/dashboard',
        staticFileGlobsIgnorePatterns: [/\.map$/, /mix-manifest\.json$/, /manifest\.json$/, /service-worker\.js$/],
        runtimeCaching: [
            {
                urlPattern: /^https:\/\/fonts\.googleapis\.com\//,
                handler: 'networkFirst'
            },
            {
                urlPattern: /^https:\/\/www\.thecocktaildb\.com\/images\/media\/drink\/(\w+)\.jpg/,
                handler: 'networkFirst'
            }
        ],
        // importScripts: ['./js/push_message.js']
    })
);

// module.exports.plugins = plugins;

