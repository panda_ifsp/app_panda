@extends('layouts.main')

@section('page_title')

{{'Dashboard'}}

@endsection

@extends('components.navbar')

@section('content')
@if($livros_fatiados)
<div class="container-fluid mt-1">
    <div class="col-md-12">
        <div class="row">
            <div class="carousel col-12" data-flickity='{ "wrapAround": true }'>

                @foreach($livros_fatiados as $livros_all)
                <div class="carousel-cell   container-fluid">
                    <div class="row">
                        @foreach($livros_all as $livro)

                        <div class="col-md-4 col-sm-4 livro">
                            <div class="card">
                                <label for="falar_{{$livro->id}}">
                                    <!-- Imagem do Livro Blob -->
                                    <img src="{{$livro->img_data}}" class="card-img-top livro-img-data">
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <!-- descricao -->
                                            <h5 class="card-title livro-descricao">{{$livro->descricao}}</h5>
                                        </div>	

                                        <div class="text-right">
                                            <input id="falar_{{$livro->id}}" class="d-none" onclick='responsiveVoice.speak("{{$livro->descricao}}","Brazilian Portuguese Male");' type="button" value="fala" />

                                        </div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</div>
<!-- @else -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-6 m-auto">
                <img class="img-warning m-auto" src="{{ asset('img/icons/panda-bear.svg') }}" style="width: 20rem; display:block;">
                <h1 class="text-center">Você não possui livros ainda!!!</h1>
                <h3 class="text-center">Para adicionar um novo livro, <a class="btn btn-info" href="{{ route('create_livro') }}">clique aqui</a></h3>
            </div>
        </div>
    </div>
<!-- @endif -->
@endsection

@section('service-worker')
    <script>
        // self.addEventListener('activate', function(event) {

        // var cacheWhitelist = ["/","/dashboard", "css/app.css", "css/croppie.css", "css/flickity.min.css", "css/swiper.min.css", "img/icons/add-button-inside-black-circle.svg", "img/icons/back.svg", "img/icons/book.svg", "img/icons/books.svg", "img/icons/cancel.svg", "img/icons/clipboard.svg", "img/icons/cloud-computing.svg", "img/icons/delete.svg", "img/icons/edit.svg", "img/icons/error.svg", "img/icons/home.svg", "img/icons/logout.svg", "img/icons/panda-bear.svg", "img/icons/settings.svg", "img/icons/user.svg", "img/icons/wink.svg", "js/app.js", "js/croppie.js", "js/croppie.min.js", "js/flickity.pkgd.min.js", "js/image-upload.js", "js/loader.js", "js/service-worker.js", "js/swiper.min.js", "js/voice.js", "js/web-cache-local-storage.js", "service-worker.js"];

        // event.waitUntil(
        // // Retrieving all the keys from the cache.
        // caches.keys().then(function(cacheNames) {
        //     return Promise.all(
        //     // Looping through all the cached files.
        //     cacheNames.map(function(cacheName) {
        //         // If the file in the cache is not in the whitelist
        //         // it should be deleted.
        //         if (cacheWhitelist.indexOf(cacheName) === -1) {
        //         return caches.delete(cacheName);
        //         }
        //     })
        //     );
        // })
        // );
        // });
    
    if ('serviceWorker' in navigator ) {
        window.addEventListener('load', function() {
            navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
                // Registration was successful
                console.log('ServiceWorker registration successful with scope: ', registration.scope);
            }, function(err) {
                // registration failed :(
                console.log('ServiceWorker registration failed: ', err);
            });
        });
    }
    </script>
@endsection