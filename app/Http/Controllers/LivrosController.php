<?php

namespace App\Http\Controllers;


use App\Livros;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Auth;
use Illuminate\Support\Facades\DB;

class LivrosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('livros.edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('livros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_livro =  new Livros();
        $new_livro->idUser = auth()->user()->id;
        if ($request->idPai != "") {
            $new_livro->idPai = $request->idPai;
        }
        $new_livro->img_data = $request->img_data;
        $new_livro->descricao = $request->description;
        $new_livro->save();

        return redirect()->route('edit');
    }
    /**

     * Display the specified resource.
     *
     * @param  \App\Livros  $livros
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // die();

        if ($codFather = $request->query('father')) {
            $livros = Livros::where([
                ['idUser', '=', auth()->user()->id],
                ['idPai', '=', $codFather]
            ])->orderBy('id')->get();
        } else {
            $livros = Livros::where([
                ['idUser', '=', auth()->user()->id],
                ['idPai', '=', Null]
            ])->orderBy('id')->get();
        }

        $livros_all = [];

        foreach ($livros as $livro) {
            $existeFilho = Livros::where('idPai', '=', $livro->id)->orderBy('id')->get();

            if (count($existeFilho) == 0) {
                $livro->folha = true;
            } else {
                $livro->folha = false;
            }

            $livros_all[] = $livro;
        }


        $livros_fatiados = [];

        for ($i = 0; $i < count($livros_all); $i += 2) {
            $livros_fatiados[] =  array_slice($livros_all, $i, 2);
        }

        return view('dashboard', compact('livros_fatiados', 'livros_all'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Livros  $livros
     * @return \Illuminate\Http\Response
     */
    public function edit(Livros $livros, Request $request)
    {
        if ($codFather = $request->query('father')) {
            $livros_all = Livros::where([
                ['idUser', '=', auth()->user()->id],
                ['idPai', '=', $codFather]
            ])->orderBy('id')->get();
        } else {
            $livros_all = Livros::where([
                ['idUser', '=', auth()->user()->id],
                ['idPai', '=', Null]
            ])->orderBy('id')->get();
        }

        return view('edit', compact('livros_all'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Livros  $livros
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $livros_all = Livros::findOrFail($request->id);
        $livros_all->descricao = $request->descricao;
        $livros_all->save();
        return redirect()->route('edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Livros  $livros
     * @return \Illuminate\Http\Response
     */
    public function destroy(Livros $livros, Request $request)
    {
        Livros::destroy($request->id);

        return redirect()->route('edit');
    }

    public function confirm_pass(Request $request)
    {
        return (Hash::check($request->password, $request->user()->password)) ? 1 : 0;
    }
}
