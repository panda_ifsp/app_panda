<?php

namespace App\Http\Controllers;

use App\Livros;
use Illuminate\Http\Request;
// use Illuminate\Foundation\Auth\User as Auth

class ReconhecimentoFacialController extends Controller
{
    public function index(Request $request){
         if ($codFather = $request->query('father')) {
            $livros = Livros::where([
                ['idUser', '=', auth()->user()->id],
                ['idPai', '=', $codFather]
            ])->orderBy('id')->get();
        } else {
            $livros = Livros::where([
                ['idUser', '=', auth()->user()->id],
                ['idPai', '=', Null]
            ])->orderBy('id')->get();
        }

        $livros_all = [];

        foreach ($livros as $livro) {
            $existeFilho = Livros::where('idPai', '=', $livro->id)->orderBy('id')->get();

            if (count($existeFilho) == 0) {
                $livro->folha = true;
            } else {
                $livro->folha = false;
            }

            $livros_all[] = $livro;
        }


        $livros_fatiados = [];

        for ($i = 0; $i < count($livros_all); $i += 2) {
            $livros_fatiados[] =  array_slice($livros_all, $i, 2);
        }


        return view('livros.ReconhecimentoFacial', compact('livros_fatiados', 'livros_all'));
    }

    public function cadeira () {
        return view('cadeira.cadeira');
    }

    public function practice(){
        return view('livros.practice');
    }
}
