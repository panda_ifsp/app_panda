<?php

namespace App\Http\Controllers;

use App\Livros;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function settings_toggle(Request $request)
    {
        $user = User::findOrFail($request->user()->id);

        if ($request->toggle == "speak") {
            $user->speak = !($user->speak);
            $user->save();
            return ($user->speak) ? 1 : 0;
        } else if ($request->toggle == "swype") {
            $user->swype = !($user->swype);
            $user->save();
            return ($user->swype) ? 1 : 0;
        } else if ($request->toggle == "invert") {
            $user->inverted = !($user->inverted);
            $user->save();
            return ($user->inverted) ? 1 : 0;
        }
    }

    public function UpdateNameEmail(Request $request)
    {
        $user = User::findOrFail($request->user()->id);

        if($request->nome != ''){
            $user->name = $request->nome;
        }
        
        if($request->email != ''){
            $user->email = $request->email;
        }   
        $user->save();

        return ($user);
    }

    public function UpdateSenha(Request $request)
    {
        if(!(Hash::check($request->current_password, $request->user()->password))){
            return 0;
        }

        if($request->new_password != $request->confirm_new_password){
            return 0;
        }

        $user = User::findOrFail($request->user()->id);
        $user->password = $request->new_password;
        $user->save();

        return 1;
    }

}
