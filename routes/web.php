<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
})->name('welcome');


Route::get('site.manifest', function(){
	return Response::make(View::make('site.manifest')->render(), 200,
		array(
			'Content-Type' => 'text/cache-manifest',
			'Cache-Control'=>'no-cache, must-revalidate, no-store'
		)
	);
});


//Route::domain('panda.itp.ifsp.edu.br')->group(function () {
Auth::routes();
Route::get('/update/email_senha', 'UserController@UpdateNameEmail')->name('alterar_email_senha');
Route::post('/update/password', 'UserController@UpdateSenha')->name('alterar_senha');

Route::get('/suporte', 'HomeController@index')->name('suporte');
Route::get('/settings', 'HomeController@index')->name('settings');

Route::get('/reconhecimento-dos-movimentos-faciais','ReconhecimentoFacialController@index')->name('face_recognition');
Route::get('/practice', 'ReconhecimentoFacialController@practice')->name('practice');
Route::get('/movimento-cadeira-de-rodas', 'ReconhecimentoFacialController@cadeira')->name('cadeira');

Route::get('/dashboard', 'LivrosController@show')->name('dashboard')->middleware('auth');

Route::get('/edit', 'LivrosController@edit')->name('edit')->middleware('auth');
Route::get('/update', 'LivrosController@update')->name('update')->middleware('auth');
Route::get('/update/localstorage', 'LivrosController@atualizarLocalStorage')->name('update_local_storage')->middleware('auth');
Route::get('/create', 'LivrosController@create')->name('create_livro')->middleware('auth');
Route::post('/destroy', 'LivrosController@destroy')->name('destroy_livro')->middleware('auth');
Route::post('/store_livro', 'LivrosController@store')->name('store_livro')->middleware('auth');
Route::get('/biblioteca', 'LivrosController@biblioteca')->name('biblioteca')->middleware('auth');
Route::post('/confirm_pass', 'LivrosController@confirm_pass')->name('confirm_pass');
Route::post('/settings_toggle', 'UserController@settings_toggle')->name('settings_toggle');


//});
